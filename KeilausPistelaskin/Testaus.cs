﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeilausPisteLaskinOhjelma;
using NUnit.Framework;

namespace KeilausPistelaskin
{
    public class Testaus
    {
        [TestFixture]
        public class KeilausPistelaskuriTest
        {
            public Keilaaja keilaaja = null;
            Random random = new Random();

            [SetUp]
            public void TestienAlustaja()
            {
                this.keilaaja = new Keilaaja();
                
            }

            [Test]
            public void FirstTest()
            {
                Assert.That(1, Is.EqualTo(1));
            }

            [Test]
            public void kaataaNollaViivaKymmenenKeilaa()
            {
                int keilat = 10;
                int kaadetutKeilat = random.Next(keilat + 1);

                Assert.That(0, Is.LessThanOrEqualTo(kaadetutKeilat));
                Assert.That(10, Is.GreaterThanOrEqualTo(kaadetutKeilat));
            }

            [Test]
            public void voiKaataaKymmenen()
            {
                int keilat = 10;
                int kymppi = 0;
                while(kymppi == 0)
                {
                    int kaadetutKeilat = random.Next(keilat + 1);
                    if (kaadetutKeilat == 10)
                    {
                        kymppi = 1;
                    }
                }
                Assert.That(1, Is.EqualTo(kymppi));
            }
        }
    }
}
