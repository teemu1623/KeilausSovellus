﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeilausPisteLaskinOhjelma
{
    class Program
    {
        static void Main(string[] args)
        {
            Keilaaja keilaaja1 = new Keilaaja();
            System.Threading.Thread.Sleep(50);
            Keilaaja keilaaja2 = new Keilaaja();
            keilaaja1.Nimi = "Pekka";
            keilaaja2.Nimi = "Pasi";
            int x = 0;

            for (int i = 0; i < 20; i = i + 2)
            {
                keilaaja1.simuloiHeittoVuoro(i);
                keilaaja1.laskeVuoronPisteet();
                keilaaja1.laskeYhteipisteet();

                Console.Clear();
                Console.WriteLine((x + 1) + ". Kierros");
                keilaaja1.tulostaTulokset();
                keilaaja2.tulostaTulokset();
                System.Threading.Thread.Sleep(1000);
                

                keilaaja2.simuloiHeittoVuoro(i);
                keilaaja2.laskeVuoronPisteet();
                keilaaja2.laskeYhteipisteet();

                Console.Clear();
                Console.WriteLine((x + 1) + ". Kierros");
                keilaaja1.tulostaTulokset();
                keilaaja2.tulostaTulokset();
                System.Threading.Thread.Sleep(1000);

                x++;
            }
            if(keilaaja1.annaKokonaispisteet() > keilaaja2.annaKokonaispisteet())
                Console.WriteLine("\n\n " + keilaaja1.Nimi + " voitti!");
            else
                Console.WriteLine("\n\n " + keilaaja2.Nimi + " voitti!");
        }
    }
}
