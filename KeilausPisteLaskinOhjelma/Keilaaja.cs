﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeilausPisteLaskinOhjelma
{
    public class Keilaaja
    {
        public string Nimi { get; set; }

        Random random = new Random();
        public int[] tulos = new int[25];
        public int[] pisteet = new int[11];
        public int[] yhteispisteet = new int[10];

        public int heitaKeilaPalloa(int keilat)
        {
            int kaadetutKeilat = random.Next(keilat + 1);
            //Console.WriteLine(Nimi + " kaatoi " + kaadetutKeilat + " keilaa");
            return kaadetutKeilat;
        }

        public void simuloiHeittoVuoro(int heitto)
        {
            if (heitto < 18)
            {
                tulos[heitto] = heitaKeilaPalloa(10);
                if (tulos[heitto] != 10) tulos[heitto + 1] = heitaKeilaPalloa(10 - tulos[heitto]);
            }
            //Viimeinen kolmen heiton kierros
            else
            {
                tulos[heitto] = heitaKeilaPalloa(10);
                tulos[heitto + 1] = heitaKeilaPalloa(10 - tulos[heitto]);
                if (tulos[heitto + 1] == 10 || tulos[heitto] + tulos[heitto + 1] == 10)
                    tulos[heitto + 2] = heitaKeilaPalloa(10);
                else if (tulos[heitto + 1] == 10)
                    tulos[heitto + 2] = heitaKeilaPalloa(10);
                else
                    tulos[heitto + 2] = heitaKeilaPalloa(10 - tulos[heitto] - tulos[heitto + 1]);
            }

        }

        public int annaHeitonPisteet(int heitto)
        {
            return tulos[heitto];
        }

        public void laskeVuoronPisteet()
        {
            int x = 0;
            int i = 0;
            while (x < 10)
            {
                //Kaato
                if (tulos[i] == 10)
                {
                    if (tulos[i + 2] == 10) pisteet[x] = tulos[i] + tulos[i + 2] + tulos[i + 4];
                    else pisteet[x] = tulos[i] + tulos[i + 2] + tulos[i + 3];
                }
                //Paikko
                else if (tulos[i] + tulos[i + 1] == 10)
                    pisteet[x] = tulos[i] + tulos[i + 1] + tulos[i + 2];
                //Jättö
                else pisteet[x] = tulos[i] + tulos[i + 1];
                //Console.WriteLine("pisteet " + pisteet[x]);               
                x++;
                i = i + 2;
            }
            pisteet[9] = tulos[18] + tulos[19] + tulos[20];
        }

        public void laskeYhteipisteet()
        {
            for (int x = 0; x < 10; x++)
            {
                yhteispisteet[x] = 0;
            }
            for (int x = 0; x < 10; x++)
            {
                if (pisteet[x] == 0 && pisteet[x + 1] == 0)
                    yhteispisteet[x] = 0;
                else
                {
                    if (x > 0 && x != 10)
                        yhteispisteet[x] += yhteispisteet[x - 1] + pisteet[x];
                    else if (x == 10)
                        yhteispisteet[x] += yhteispisteet[x - 1] + pisteet[x];
                    else
                        yhteispisteet[x] += pisteet[x];
                }
            //Console.WriteLine("Yhteispisteet " + yhteispisteet[x]);
            }
        }
        public void tulostaTulokset()
        {
            Console.WriteLine();

            //Ylärivi
            Console.Write("Nimi:     ");
            for (int heitto = 0; heitto < 20; heitto = heitto + 2 )
            {
                if (tulos[heitto] + tulos[heitto + 1] != 10)
                    Console.Write("|" + tulos[heitto] + " " + tulos[heitto + 1]);
                else if (tulos[heitto] == 10)
                    Console.Write("|X  ");
                else
                    Console.Write("|" + tulos[heitto] + " /");
            } 
            Console.Write(" " + tulos[20]);
            Console.WriteLine("|  Pisteet:");

            //Alarivi            
            Console.Write("   " + Nimi);
            for (int i = 0; i < 7 - Nimi.Length; i++)
            {
                Console.Write(" ");
            }

            for (int i = 0; i < 9; i++)
            {
                if (yhteispisteet[i] > 100) Console.Write("|" + yhteispisteet[i]);
                else if (yhteispisteet[i] >= 10) Console.Write("| " + yhteispisteet[i]);
                else Console.Write("|  " + yhteispisteet[i]);
            }

            //Viimeinen kierros
            if (yhteispisteet[9] >= 100) Console.Write("|  " + yhteispisteet[9]);
            else if (yhteispisteet[9] >= 10) Console.Write("|   " + yhteispisteet[9]);
            else Console.Write("|    " + yhteispisteet[9]);

            int kokonaispisteet = 0;
            for (int i = 0; i < 10; i++)
            {
                kokonaispisteet += pisteet[i];
            }
            Console.Write("|    "+ kokonaispisteet);
            
        }
        public int annaKokonaispisteet()
        {
            int kokonaispisteet = 0;
            for (int i = 0; i < 10; i++)
            {
                kokonaispisteet += pisteet[i];
            }
            return kokonaispisteet;
        }
    }
}
